package com.faci.lisa.showme;

import android.graphics.drawable.Drawable;

/**
 * Created by lfaci on 23/03/18.
 */

public class color {
    private String name;
    private int value;

    public color(String name, int color) {
        this.name = name;
        this.value = color;
    }

    private color() {
    }

    public String getName() {
        return name;
    }

    public int getValue() {
        return value;
    }

}
