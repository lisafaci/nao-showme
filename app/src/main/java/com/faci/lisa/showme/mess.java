package com.faci.lisa.showme;

/**
 * Created by lfaci on 10/05/18.
 */

public class mess {

    private String message;

    public mess(String name) {
        this.message = name;
    }

    private mess() {
    }

    public String getMessage() {
        return message;
    }

}
