package com.faci.lisa.showme;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
//import android.support.multidex.MultiDex;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.aldebaran.qi.Session;
import com.aldebaran.qi.helper.proxies.ALMotion;
import com.aldebaran.qi.helper.proxies.ALRobotPosture;
import com.aldebaran.qi.helper.proxies.ALTextToSpeech;
import com.google.firebase.auth.FirebaseAuth;

import java.util.List;

public class HomeActivity extends AppCompatActivity {
    private static final String TAG = "MyActivity";
    private  FirebaseAuth auth;
    private static  final int MY_PERMISSION_REQUEST_CHANGE_WIFI_STATE = 123;
    Button varBtn;
    TextView wifi;
    WifiManager mWifiManager;
    List<ScanResult> wifiList;
    private ALMotion motion;
    private ALTextToSpeech speech;
    private ALRobotPosture posture;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        Button add = (Button) findViewById(R.id.addIt);
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onPause();
                Intent i = new Intent(getApplicationContext(), HomeTask.class);
                startActivity(i);
            }
        });

        // Status Bar
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
            this.getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.black));

        wifi = (TextView) findViewById(R.id.wifi);
        // Récupère le user / firebase
        auth = FirebaseAuth.getInstance();
        varBtn = (Button) findViewById(R.id.idBtn);
        varBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                    if (PackageManager.PERMISSION_GRANTED != ContextCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.ACCESS_FINE_LOCATION)) {
                        requestPermissions(new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                            MY_PERMISSION_REQUEST_CHANGE_WIFI_STATE);
                } else {
                    Toast.makeText(getApplicationContext(), "Method Called!", Toast.LENGTH_SHORT).show();
                    MyWifiMethod();
                }
            }
        });



        /* Navigation Bar (Bottom) */
        BottomNavigationView bottomNavigationView = (BottomNavigationView) findViewById(R.id.navigation);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.action_item1:
                        onPause();
                        Intent intent = new Intent(getApplicationContext(), EyesColor.class);
                        startActivity(intent);
                        Toast.makeText(getApplicationContext(), "eyes", Toast.LENGTH_SHORT).show();
                        break;
                    case R.id.action_item2:
                        Toast.makeText(getApplicationContext(), "général", Toast.LENGTH_SHORT).show();
                        break;
                    case R.id.action_item3:
                        onPause();
                        Intent speak = new Intent(getApplicationContext(), Speek.class);
                        startActivity(speak);
                        Toast.makeText(getApplicationContext(), "speek", Toast.LENGTH_SHORT).show();
                        break;
                }
                return true;
            }
        });

        /* Custum Action Bar */
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.mipmap.ic_launcher_foreground);
        getSupportActionBar().setTitle("  NAO");
        ColorDrawable colorDrawable = new ColorDrawable(Color.parseColor("#000000"));
        getSupportActionBar().setBackgroundDrawable(colorDrawable);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case MY_PERMISSION_REQUEST_CHANGE_WIFI_STATE: {
                if (grantResults.length >= 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    MyWifiMethod();
                } else {
                    Toast.makeText(getApplicationContext(), "You don't have require permission to make the Action", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    private void    MyWifiMethod() {
        mWifiManager = (WifiManager) getApplicationContext().getSystemService(WIFI_SERVICE);
        getApplicationContext().registerReceiver(mWifiScanReciever,
                new IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION));
        mWifiManager.startScan();
    }


    private final   BroadcastReceiver mWifiScanReciever = new BroadcastReceiver() {

        StringBuilder sb = new StringBuilder();
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION)) {

                wifiList = mWifiManager.getScanResults();
                sb = new StringBuilder();
                sb.append("\n" + "Number Of Wifi connections :" + wifiList.size() + "\n");
                for (int i = 0; i < wifiList.size(); i++) {
                    sb.append(new Integer(i + 1).toString() + ". ");
                    sb.append("SSID" + (wifiList.get(i).SSID).toString() + "\n");
                    sb.append("BSSID" + (wifiList.get(i).BSSID).toString() + "\n");
                    sb.append("Capabilities" + (wifiList.get(i).capabilities).toString() + "\n");
                    sb.append("level" + (wifiList.get(i).level) + "\n");
                    sb.append("describeContents" + (wifiList.get(i).describeContents()) + "\n");
                    sb.append("\n\n");
                    wifi.setId(i);
                }
                wifi.setText(sb);
            }
        }
    };


    // pour la création de Module avec NAO

    /*public void showPlaceHolderFragment(){
        if (session != null) {
            Log.i(TAG, "showing fragment");
            // Call onProxyReady when a ALProxy is correctly initiated, or onProxyException otherwise.
            try {
                // Create an instance of the needed ALProxy: ALMotion, ALTextToSpeech and ALRobotPosture
                motion = new ALMotion(session);
                speech = new ALTextToSpeech(session);
                posture = new ALRobotPosture(session);
                if (motion != null) {
                    Toast toast = Toast.makeText(getApplicationContext(), "motion", Toast.LENGTH_LONG);
                    toast.show();
                } if (speech != null) {
                    Toast toast = Toast.makeText(getApplicationContext(), "speech", Toast.LENGTH_LONG);
                    toast.show();
                } if (posture != null) {
                    Toast toast = Toast.makeText(getApplicationContext(), "posture", Toast.LENGTH_LONG);
                    toast.show();
                }
                //getSupportFragmentManager().beginTransaction().show(this.placeholderFragment).commit();
            }
            catch(Exception e){
                e.printStackTrace();
            }
        }
    }*/

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.context_menu, menu);
        return true;
    }

   /* @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getGroupId()) {
            case R.id.deconection:
                auth.signOut();
                onDestroy();
                Intent main = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(main);
                return true;
            default:
                auth.signOut();
                onDestroy();
                Intent maine = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(maine);
                return super .onOptionsItemSelected(item);
        }
    }

    public class Client {

        public Client() {

            Socket socket;
            try {
                socket = new Socket("162.198.8.100",9559);
                socket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    private class ConnectionTask extends AsyncTask<String, Integer, Session> {

        private ProgressDialog dialog;
        private Activity myActivity;

        public ConnectionTask(Activity activity){
            myActivity = activity;
            dialog = new ProgressDialog(activity);
        }

        @Override
        protected void onPreExecute() {
            this.dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            this.dialog.setMessage("connecting_message");
            //this.dialog.show();
        }

        protected Session doInBackground(String... url) {
            try {
                // Create a new Session
                session = new Session();
                // Connect to the robot in the given IP address
                session.connect(url[0]).get();
                // Set the callback method in case connection is lost.
                session.onDisconnected("onDisconnected", myActivity);
            }
            catch (Exception e){
                session = null;
                e.printStackTrace();
            }
            return session;
        }

        protected void onPostExecute(Session createdSession) {
            String message = "not_connected";
            if (createdSession != null) {
                message = "connected";
                ((Button) findViewById(R.id.connect_button)).setText(getString(R.string.disconnect));
                findViewById(R.id.robot_ip).setEnabled(false);
                ((HomeActivity) myActivity).showPlaceHolderFragment();
            }
            Toast toast = Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG);
            toast.show();
            if (dialog.isShowing()) {
                dialog.dismiss();
            }
        }


    }*/
}
