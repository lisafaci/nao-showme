package com.faci.lisa.showme;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.annotation.RequiresApi;
//import android.support.multidex.MultiDex;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.madrapps.pikolo.HSLColorPicker;
import com.madrapps.pikolo.listeners.SimpleColorSelectionListener;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;


public class EyesColor extends AppCompatActivity {

    private  FirebaseAuth auth;
    private FirebaseDatabase dauth;
    private int colore;
    public GridView gridView;
    public int [] colortab = new int[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    public String [] data = new String[] {null, null, null, null, null, null, null, null, null, null};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_eyes_color);

        auth = FirebaseAuth.getInstance();
        dauth = FirebaseDatabase.getInstance();
        FirebaseAuth auth = FirebaseAuth.getInstance();
        final FirebaseUser mFirebaseUser = auth.getCurrentUser();
        final String mUserId = mFirebaseUser.getUid();
        FirebaseDatabase dauth = FirebaseDatabase.getInstance();
        final DatabaseReference ref = dauth.getReference();
        final int[] i = {0};

        // init la gridView
        gridView = (GridView) findViewById(R.id.idGridView);
        CustomGrid adapter = new CustomGrid(EyesColor.this, data, colortab);
        gridView.setAdapter(adapter);

        // afficher dans le grid view les couleurs de la base de donnée.
        ref.child("users").child(mUserId).child("colors")
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                            color user = snapshot.child("color").getValue(color.class);
                            data[i[0]] = user.getName();
                            colortab[i[0]] = user.getValue();
                            i[0]++;
                        }
                        if (data[0] == null || colortab[0] == 0)
                        {
                            data = new String[]{"flash", "circle eyes", "disco", "blink", "Mischievous", "cautious", "thinking", "lought", "Angry", "happy"};
                            colortab = new int[]{-15204679, -15147812, -2352938, -1, -5760804, -91386, -16378114, -791529, 129522, -11156920};
                        }
                        CustomGrid adapter = new CustomGrid(EyesColor.this, data, colortab);
                        gridView.setAdapter(adapter);
                    }
                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                    }
                });

        // si l'on click sur une coleur
        gridView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, final View view, int position, long id) {
                CharSequence change[] = new CharSequence[] {"modifier", "reset"};
                final AlertDialog.Builder builder = new AlertDialog.Builder(EyesColor.this);
                builder.setCancelable(true);
                builder.setItems(change, new DialogInterface.OnClickListener() {
                    @RequiresApi(api = Build.VERSION_CODES.O)
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:
                                // modifier
                                LinearLayout tv = (LinearLayout) view;
                                tv.getChildAt(0).setBackgroundColor(colore);
                                final int couleur = getBackgroundColor(tv.getChildAt(0));
                                final String name = ((TextView)tv.getChildAt(1)).getText().toString();

                                // modification de la base de donné
                                ref.child("users").child(mUserId).child("colors")
                                        .addListenerForSingleValueEvent(new ValueEventListener() {
                                            @Override
                                            public void onDataChange(DataSnapshot dataSnapshot) {
                                                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                                                    color user = snapshot.child("color").getValue(color.class);
                                                    if (user.getName().equals(name)) {
                                                        String key = snapshot.getKey();
                                                        HashMap<String, Object> result = new HashMap<>();
                                                        result.put("name", name);
                                                        result.put("value", couleur);
                                                        ref.child("users").child(mUserId).child("colors").child(key).child("color").updateChildren(result);
                                                    }
                                                }
                                            }
                                            @Override
                                            public void onCancelled(DatabaseError databaseError) {
                                            }
                                        });
                                break;
                            case 1:
                                // reset à blanc
                                LinearLayout tvd = (LinearLayout) view;
                                tvd.getChildAt(0).setBackgroundColor(Color.parseColor("#FFFFFF"));
                                final int couleurWhite = getBackgroundColor(tvd.getChildAt(0));
                                final String nameWhite = ((TextView)tvd.getChildAt(1)).getText().toString();

                                // modification de la base de donné
                                ref.child("users").child(mUserId).child("colors")
                                        .addListenerForSingleValueEvent(new ValueEventListener() {
                                            @Override
                                            public void onDataChange(DataSnapshot dataSnapshot) {
                                                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                                                    color user = snapshot.child("color").getValue(color.class);
                                                    if (user.getName() == nameWhite) {
                                                        String key = snapshot.getKey();
                                                        HashMap<String, Object> result = new HashMap<>();
                                                        result.put("name", nameWhite);
                                                        result.put("value", couleurWhite);
                                                        ref.child("users").child(mUserId).child("colors").child(key).child("color").updateChildren(result);
                                                    }
                                                }
                                            }
                                            @Override
                                            public void onCancelled(DatabaseError databaseError) {
                                            }
                                        });
                                break;
                        }

                    }});
                AlertDialog dialog = builder.create();
                dialog.show();
                return false;
            }
        });

        final ImageView image = (ImageView) findViewById(R.id.imageView);

        final HSLColorPicker colorPicker = (HSLColorPicker) findViewById(R.id.colorPicker);
        colorPicker.setColorSelectionListener(new SimpleColorSelectionListener() {
            @Override
            public void onColorSelected(int color) {
                // Do whatever you want with the color
                image.getBackground().setColorFilter(color, PorterDuff.Mode.MULTIPLY);
                colore = color;
            }
        });

        // Action Bar
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("  EYES");
        ColorDrawable colorDrawable = new ColorDrawable(Color.parseColor("#000000"));
        getSupportActionBar().setBackgroundDrawable(colorDrawable);

        // Status Bar
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            this.getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.black));
        }
    }

    private static int getBackgroundColor(View view) {
        Drawable drawable = view.getBackground();
        if (drawable instanceof ColorDrawable) {
            ColorDrawable colorDrawable = (ColorDrawable) drawable;
            if (Build.VERSION.SDK_INT >= 11) {
                return colorDrawable.getColor();
            }
            try {
                Field field = colorDrawable.getClass().getDeclaredField("mState");
                field.setAccessible(true);
                Object object = field.get(colorDrawable);
                field = object.getClass().getDeclaredField("mUseColor");
                field.setAccessible(true);
                return field.getInt(object);
            } catch (NoSuchFieldException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        return 0;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.context_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getGroupId()) {
            case R.id.deconection:
                auth.signOut();
                finish();
                Intent main = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(main);
                return true;
            case android.R.id.home:
                finish();
                Intent back = new Intent(getApplicationContext(), HomeActivity.class);
                startActivity(back);
                return true;
            default:
                finish();
                return true;
        }
    }

    /*public void setDataA(String Data[]) {
        this.data = Data;
    }

    public void setColorTab(int Data[]) {
        this.colortab = Data;
    }

    public String []getDataA() {
        return this.data;
    }

    public int []getColorTab() {
        return this.colortab;
    }*/
}
