package com.faci.lisa.showme;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
//import android.support.multidex.MultiDex;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.aldebaran.qi.helper.proxies.ALMemory;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.w3c.dom.Text;

import java.util.HashMap;

/**
 * Created by lfaci on 20/03/18.
 */

public class NewActivite extends AppCompatActivity {

    public int [] colortab = new int[10];
    public String [] data = new String[10];
    public String [] dataSpeek = new String[10];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.newactivity);
        //MultiDex.install(this);

        FirebaseAuth auth = FirebaseAuth.getInstance();
        final FirebaseUser mFirebaseUser = auth.getCurrentUser();
        final String mUserId = mFirebaseUser.getUid();
        FirebaseDatabase dauth = FirebaseDatabase.getInstance();
        final DatabaseReference ref = dauth.getReference();

        // Status Bar
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
            this.getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.black));

        /* Custum Action Bar */
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        ColorDrawable colorDrawable = new ColorDrawable(Color.parseColor("#000000"));
        getSupportActionBar().setBackgroundDrawable(colorDrawable);

        //afficher dans le grid view les couleurs de la base de donnée.


        // Pour les yeux
        final GridView gridView = new GridView(NewActivite.this);
        final CustomGrid adapter = new CustomGrid(NewActivite.this, data, colortab);
        gridView.setAdapter(adapter);
        final int i[] = {0};
        ref.child("users").child(mUserId).child("colors")
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                            color user = snapshot.child("color").getValue(color.class);
                            data[i[0]] = user.getName();
                            colortab[i[0]] = user.getValue();
                            i[0]++;
                        }
                        //gridView
                        CustomGrid adapter = new CustomGrid(NewActivite.this, data, colortab);
                        gridView.setAdapter(adapter);
                    }
                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                    }
                });
        final TextView lv = new TextView(NewActivite.this);

        //pour Les text
        final GridView gridViewSpeek = new GridView(NewActivite.this);
        final int x[] = {0};
        ref.child("users").child(mUserId).child("items")
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                            mess user = snapshot.getValue(mess.class);
                            dataSpeek[x[0]] = user.getMessage();
                            x[0]++;
                        }
                        //gridView
                        SpeekGrid adapterSpeek = new SpeekGrid(NewActivite.this, dataSpeek);
                        gridViewSpeek.setAdapter(adapterSpeek);
                    }
                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                    }
                });
        final TextView lvSpeek = new TextView(NewActivite.this);

        final LinearLayout longtouch = (LinearLayout) findViewById(R.id.longtouch);
        longtouch.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                // Create out AlterDialog
                AlertDialog.Builder builder = new AlertDialog.Builder(NewActivite.this);
                builder.setTitle("This will end the activity");
                builder.setItems(R.array.items, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0 :
                                AlertDialog.Builder eyesbuilder = new AlertDialog.Builder(NewActivite.this);
                                eyesbuilder.setTitle("Choose your color");
                                gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                    @Override
                                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                        // Get the GridView selected/clicked item text
                                        LinearLayout tv = (LinearLayout) view;
                                        String selectedItem = ((TextView)tv.getChildAt(1)).getText().toString();
                                        lv.setText(selectedItem);
                                    }
                                });
                                eyesbuilder.setView(gridView);
                                AlertDialog eyesdialog = eyesbuilder.create();
                                eyesdialog.show();
                                longtouch.addView(lv);
                                break;
                            case 1 :
                                AlertDialog.Builder speekbuilder = new AlertDialog.Builder(NewActivite.this);
                                speekbuilder.setTitle("Choose your text");
                                gridViewSpeek.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                    @Override
                                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                        // Get the GridView selected/clicked item text
                                        LinearLayout tv = (LinearLayout) view;
                                        String selectedItem = ((TextView)tv.getChildAt(1)).getText().toString();
                                        lvSpeek.setText(selectedItem);
                                    }
                                });
                                speekbuilder.setView(gridViewSpeek);
                                AlertDialog speekdialog = speekbuilder.create();
                                speekdialog.show();
                                longtouch.addView(lvSpeek);
                                break;
                            case 2 :
                                TextView endm = new TextView(NewActivite.this);
                                endm.setText("move");
                                longtouch.addView(endm);
                                break;
                        }
                    }
                });
                AlertDialog dialog = builder.create();
                dialog.show();
                return false;
            }
        });
    }

    private final class CancelOnClickListener implements
            DialogInterface.OnClickListener {
        public void onClick(DialogInterface dialog, int which) {
            Toast.makeText(getApplicationContext(), "Activity will continue",
                    Toast.LENGTH_LONG).show();
        }
    }

    private final class OkOnClickListener implements
            DialogInterface.OnClickListener {
        public void onClick(DialogInterface dialog, int which) {
            //AlertExampleActivity.this.finish();
        }
    }
}