package com.faci.lisa.showme;

import android.app.Service;

import com.aldebaran.qi.Application;
import com.aldebaran.qi.DynamicObjectBuilder;
import com.aldebaran.qi.AnyObject;
import com.aldebaran.qi.Session;

/**
 * Created by lfaci on 19/05/18.
 */

import com.aldebaran.qi.*;

public class talkNao {

    private AnyObject tts;

    public void run(String[] args) throws Exception {
        String url = "tcp://nao.local:9559";
        if (args.length == 1) {
            url = args[0];
        }
        Application application = new Application(args);
        Session session = new Session();
        Future<Void> fut = session.connect(url);
        fut.get();
        tts = session.service("ALTextToSpeech");
        //tts.say();
        application.run();

    }

    public static void main(String[] args) throws Exception {
        talkNao reactor = new talkNao();
        reactor.run(args);
    }
}


