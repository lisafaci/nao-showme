package com.faci.lisa.showme;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
//import android.support.multidex.MultiDex;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

/**
 * Created by lfaci on 20/03/18.
 */

public class HomeTask extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.hometask);
        //MultiDex.install(this);

        // Action Bar
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("  ACTIVITES");
        ColorDrawable colorDrawable = new ColorDrawable(Color.parseColor("#000000"));
        getSupportActionBar().setBackgroundDrawable(colorDrawable);

        // Status Bar
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
            this.getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.black));


        Button newactivite = (Button) findViewById(R.id.idNew);
        newactivite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onPause();
                Intent newact = new Intent(getApplicationContext(), NewActivite.class);
                startActivity(newact);
            }
        });

        /* Navigation Bar (Bottom) */
        BottomNavigationView bottomNavigationView = (BottomNavigationView) findViewById(R.id.navigation);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.action_item1:
                        onPause();
                        Intent intent = new Intent(getApplicationContext(), EyesColor.class);
                        startActivity(intent);
                        Toast.makeText(getApplicationContext(), "eyes", Toast.LENGTH_SHORT).show();
                        break;
                    case R.id.action_item2:
                        Toast.makeText(getApplicationContext(), "général", Toast.LENGTH_SHORT).show();
                        break;
                    case R.id.action_item3:
                        onPause();
                        Intent speak = new Intent(getApplicationContext(), Speek.class);
                        startActivity(speak);
                        Toast.makeText(getApplicationContext(), "speek", Toast.LENGTH_SHORT).show();
                        break;
                }
                return true;
            }
        });

        Button New = (Button) findViewById(R.id.idNew);
        New.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onPause();
                Intent NewActivite = new Intent(getApplicationContext(), NewActivite.class);
                startActivity(NewActivite);
            }
        });
    }

}
