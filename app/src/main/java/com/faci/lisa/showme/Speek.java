package com.faci.lisa.showme;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
//import android.support.multidex.MultiDex;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;

import static android.view.View.GONE;

public class Speek extends AppCompatActivity {

    final ArrayList<String> keyList = new ArrayList<>();
    private  FirebaseAuth auth;
    private FirebaseDatabase dataauth;
    private int glob = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_speek);
        //MultiDex.install(this);


        dataauth = FirebaseDatabase.getInstance();
        auth = FirebaseAuth.getInstance();

        // Status Bar
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
            this.getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.black));


        final FirebaseUser mFirebaseUser = auth.getCurrentUser();
        Button save = (Button) findViewById(R.id.saveTxt);
        final TextView txtToSave = (TextView) findViewById(R.id.tosay);
        final String mUserId = mFirebaseUser.getUid();
        final DatabaseReference myRef = dataauth.getReference();

        // Set up ListView
        final ListView listView = (ListView) findViewById(R.id.listView);
        final ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, android.R.id.text1);
        listView.setAdapter(adapter);
        listView.setVisibility(GONE);
        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(final AdapterView<?> arg0, final View arg1,
                                           final int pos, final long id) {
                CharSequence change[] = new CharSequence[] {"modifier", "supprimer"};
                final AlertDialog.Builder builder = new AlertDialog.Builder(Speek.this);
                builder.setCancelable(true);
                builder.setItems(change, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // the user clicked on colors[which]
                        switch (which) {
                            case 0:
                                final String tmp = adapter.getItem(pos);
                                final AlertDialog.Builder builder = new AlertDialog.Builder(Speek.this);
                                builder.setCancelable(true);
                                final EditText text = new EditText(Speek.this);
                                builder.setView(text);
                                builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int whichButton) {

                                        // mofidier la bdd
                                        myRef.child("users").child(mUserId).child("items")
                                                .addListenerForSingleValueEvent(new ValueEventListener() {
                                                    @Override
                                                    public void onDataChange(DataSnapshot dataSnapshot) {
                                                        for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                                                            mess message = snapshot.getValue(mess.class);
                                                            if (message.getMessage().equals(tmp)) {
                                                                String key = snapshot.getKey();
                                                                HashMap<String, Object> result = new HashMap<>();
                                                                result.put("message", text.getText().toString());
                                                                myRef.child("users").child(mUserId).child("items").child(key).setValue(result);
                                                            }
                                                        }
                                                    }
                                                    @Override
                                                    public void onCancelled(DatabaseError databaseError) {
                                                    }
                                                });
                                    }
                                });
                                builder.show();
                                break;
                            case 1:
                                String s = listView.getItemAtPosition(pos).toString();
                                myRef.child("users").child(mUserId).child("items").orderByChild("message").equalTo(s)
                                        .addListenerForSingleValueEvent(new ValueEventListener() {
                                            @Override
                                            public void onDataChange(DataSnapshot dataSnapshot) {
                                                if (dataSnapshot.hasChildren()) {
                                                    DataSnapshot firstChild = dataSnapshot.getChildren().iterator().next();
                                                    firstChild.getRef().removeValue();
                                                }
                                            }

                                            @Override
                                            public void onCancelled(DatabaseError databaseError) {

                                            }
                                        });
                                break;
                        }

                    }});
                AlertDialog dialog = builder.create();
                dialog.show();
                return true;
            }
        });

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String s = txtToSave.getText().toString();
                myRef.child("users").child(mUserId).child("items").push().child("message").setValue(s);
            }
        });

        //rendre Visible la list
        final Button see = (Button) findViewById(R.id.see);

        see.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (glob == 0) {
                    listView.setVisibility(View.VISIBLE);
                    glob = 1;
                }
                else {
                    listView.setVisibility(GONE);
                    glob = 0;
                }
            }
        });

        // Use Firebase to populate the list.
        myRef.child("users").child(mUserId).child("items").addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                adapter.add((String) dataSnapshot.child("message").getValue());
                for (DataSnapshot messages : dataSnapshot.getChildren()) {
                    keyList.add(messages.getKey());
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                adapter.remove((String) dataSnapshot.child("message").getValue());
                dataSnapshot.getRef().removeValue();

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        // Action Bar
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("  SPEEK");
        ColorDrawable colorDrawable = new ColorDrawable(Color.parseColor("#000000"));
        getSupportActionBar().setBackgroundDrawable(colorDrawable);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.context_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getGroupId()) {
            case R.id.deconection:
                auth.signOut();
                onStop();
                Intent main = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(main);
                return true;
            case android.R.id.home:
                onStop();
                Intent back = new Intent(getApplicationContext(), HomeActivity.class);
                startActivity(back);
                return true;
            default:
                finish();
                return true;
        }
    }
}
